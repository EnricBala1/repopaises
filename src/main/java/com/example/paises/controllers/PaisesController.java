package com.example.paises.controllers;

import com.example.paises.componentes.PaisesModel;
import com.example.paises.componentes.PaisesSelectModel;
import com.example.paises.componentes.PaisesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Controller
@RequestMapping("paises")
public class PaisesController {


    @Autowired
    PaisesService paisesService;

    @GetMapping("/")
    public ResponseEntity<List<Object>> getAll() {
        return paisesService.getAll();
    }

    @GetMapping("/inicio")
    public String getPaises(Model model) throws IOException {
        try {
            PaisesModel[] paises = paisesService.getPaises();
            List<PaisesSelectModel> options = new ArrayList<PaisesSelectModel>();
            for (PaisesModel pais : paises) {
                UriComponentsBuilder ucb = MvcUriComponentsBuilder.fromMethodName(PaisesController.class,"servePais", pais.getName());
                String link = ucb.build().toUri().toString();
                options.add(new PaisesSelectModel(pais.getName(), link));
            }
            model.addAttribute("paises", options);
            return "paisesForm";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @GetMapping("/{nombre}")
    @ResponseBody
    public PaisesModel servePais(@PathVariable String nombre) throws Exception {
        PaisesModel pais = paisesService.getPais(nombre);
        return pais;
    }
}
