package com.example.paises.componentes;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class PaisesService {
    @Autowired
    private RestTemplate restTemplate;
    private static String url_all = "https://restcountries.eu/rest/v2/all";

    public ResponseEntity<List<Object>> getAll() {
        //Object[] paises = restTemplate.getForObject(url_all, Object[].class);
        //return Arrays.asList(paises);
        ResponseEntity<Object[]> response
                = restTemplate.getForEntity(url_all, Object[].class);
        if (response.getStatusCode() == HttpStatus.OK) {
            Object[] paises = response.getBody();
            return ResponseEntity.ok().body(Arrays.asList(paises));
        } else {
            return ResponseEntity.status(500).build();
        }
    }

    public PaisesModel[] getPaises() throws Exception {
        PaisesModel[] paises = restTemplate.getForObject(url_all, PaisesModel[].class);
        return paises;
    }
    public PaisesModel getPais(String nombre) throws Exception {
        String url = url_all.replace("all", String.format("name/%s", nombre));
        System.out.println(url);
        PaisesModel[] paises = restTemplate.getForObject(url, PaisesModel[].class);
        if (paises.length > 0) {
            return paises[0];
        } else return null;
    }
}
