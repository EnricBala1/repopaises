package com.example.paises.componentes;

import java.io.Serializable;

public class PaisesSelectModel implements Serializable {
    private String nombre;
    private String link;

    public PaisesSelectModel() {
    }

    public PaisesSelectModel(String nombre, String link) {
        this.nombre = nombre;
        this.link = link;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
